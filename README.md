## Vivid Seats Challenge

This app consumes an api rest endpoint provided by VividSeats and shows the response in a Recycler View.

In order to have a maintainable and testable app the MVVM pattern was adopted, instead of using the LiveData component the app uses observables from RxJava to improve testability.

The project has two test classes, one for testing the EventsRepository and another to test the EvenstViewModel, also a MockRetrofit implementation was created to conduct the tests.

* Libraries used:

	* Android Arch Components: ViewModel class
	* Dagger 2: Dependency Management
	* Picasso: Image Download
	* Retrofit 2: HTTP requests
	* Gson: Json Serialization/Deserialization
	* RxJava: Reactive components




