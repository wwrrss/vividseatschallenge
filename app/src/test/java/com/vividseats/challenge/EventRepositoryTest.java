package com.vividseats.challenge;

import com.vividseats.challenge.models.Card;
import com.vividseats.challenge.networking.ApiService;
import com.vividseats.challenge.repositories.EventRepository;
import com.vividseats.challenge.repositories.EventRepositoryImp;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Predicate;
import io.reactivex.observers.TestObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.MockRetrofit;
import retrofit2.mock.NetworkBehavior;

public class EventRepositoryTest {

    private EventRepository eventRepository;

    @Before
    public void setup(){
        MockApiService mockApiService = MockApiService.create();
        eventRepository = new EventRepositoryImp(mockApiService);
    }

    @Test
    public void shouldSubscribeAndReturnValue(){
        eventRepository.fetchNetworkData();
        eventRepository.getCardsObservable()
                    .test()
                    .assertSubscribed()
                    .awaitCount(1)
                    .assertNoErrors()
                    .assertValue(new Predicate<List<Card>>() {
                        @Override
                        public boolean test(List<Card> cardList) throws Exception {
                            return cardList.get(0).getTopLabel().equals(TestConstants.TEST_EVENT_LABEL);
                        }
                    });




    }
}
