package com.vividseats.challenge;

import com.vividseats.challenge.models.Card;
import com.vividseats.challenge.repositories.EventRepository;
import com.vividseats.challenge.repositories.EventRepositoryImp;
import com.vividseats.challenge.viewmodels.EventsViewModel;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import io.reactivex.functions.Predicate;

public class EventsViewModelTest {

    private EventRepository eventRepository;
    private EventsViewModel eventsViewModel;


    @Before
    public void setup(){
        MockApiService mockApiService = MockApiService.create();
        eventRepository = new EventRepositoryImp(mockApiService);
        eventsViewModel = new EventsViewModel(eventRepository);
    }

    @Test
    public void shouldSubscribeAndReturnData(){
        eventsViewModel.getCardsObservable()
                    .test()
                    .assertSubscribed()
                    .awaitCount(1)
                    .assertNoErrors()
                    .assertValue(new Predicate<List<Card>>() {
                        @Override
                        public boolean test(List<Card> cardList) throws Exception {
                            return cardList.get(0).getTopLabel().equals(TestConstants.TEST_EVENT_LABEL);
                        }
                    });
    }
}
