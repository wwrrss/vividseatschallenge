package com.vividseats.challenge;

import com.vividseats.challenge.models.Card;
import com.vividseats.challenge.models.RequestBody;
import com.vividseats.challenge.networking.ApiService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.MockRetrofit;
import retrofit2.mock.NetworkBehavior;

public class MockApiService implements ApiService {

    private BehaviorDelegate<ApiService> delegate;

    public MockApiService(BehaviorDelegate<ApiService> delegate){
        this.delegate = delegate;
    }

    public static MockApiService create(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://localhost")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        NetworkBehavior networkBehavior = NetworkBehavior.create();
        MockRetrofit mockRetrofit = new MockRetrofit
                .Builder(retrofit)
                .networkBehavior(networkBehavior)
                .build();
        BehaviorDelegate delegate = mockRetrofit.create(ApiService.class);
        MockApiService mockApiService = new MockApiService(delegate);
        return mockApiService;
    }

    @Override
    public Observable<List<Card>> getCards(RequestBody requestBody) {
        Card card = new Card();
        card.setTopLabel(TestConstants.TEST_EVENT_LABEL);
        card.setMiddleLabel("Test mid label");
        card.setStartDate(new Date());
        List<Card> cardList = new ArrayList<>();
        cardList.add(card);
        return delegate.returningResponse(cardList).getCards(new RequestBody());
    }
}
