package com.vividseats.challenge.utils;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.vividseats.challenge.repositories.EventRepository;
import com.vividseats.challenge.viewmodels.EventsViewModel;



public class EventsViewModelFactory implements ViewModelProvider.Factory {

    private EventRepository eventRepository;


    public EventsViewModelFactory(EventRepository eventRepository){
        this.eventRepository = eventRepository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new EventsViewModel(eventRepository);
    }
}
