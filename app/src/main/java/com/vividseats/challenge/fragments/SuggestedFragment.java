package com.vividseats.challenge.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.vividseats.challenge.R;
import com.vividseats.challenge.adapters.EventsRecyclerAdapter;
import com.vividseats.challenge.app.CustomApp;
import com.vividseats.challenge.models.Card;
import com.vividseats.challenge.repositories.EventRepository;
import com.vividseats.challenge.utils.EventsViewModelFactory;
import com.vividseats.challenge.viewmodels.EventsViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SuggestedFragment extends Fragment {

    @Inject
    EventRepository eventRepository;

    private EventsViewModelFactory eventsViewModelFactory;

    private EventsViewModel eventsViewModel;

    private RecyclerView suggestedRecyclerView;

    private EventsRecyclerAdapter eventsRecyclerAdapter;

    private CompositeDisposable compositeDisposable;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.suggested_fragment,container,false);
        ((CustomApp)getActivity().getApplication()).getAppComponent().inject(this);
        compositeDisposable = new CompositeDisposable();
        suggestedRecyclerView = view.findViewById(R.id.suggestedFragmentRecycler);
        eventsRecyclerAdapter = new EventsRecyclerAdapter(new ArrayList<Card>(),getContext());
        RecyclerView.LayoutManager layoutManager =new LinearLayoutManager(getActivity());
        suggestedRecyclerView.setLayoutManager(layoutManager);
        suggestedRecyclerView.setAdapter(eventsRecyclerAdapter);

        eventsViewModelFactory = new EventsViewModelFactory(eventRepository);
        eventsViewModel = ViewModelProviders.of(this, eventsViewModelFactory).get(EventsViewModel.class);
        subscribeToViewModelData();
        return view;
    }

    private void subscribeToViewModelData(){
        eventsViewModel.getCardsObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Card>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(List<Card> cardList) {
                        eventsRecyclerAdapter.addNewData(cardList);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast
                                .makeText(getActivity(),getString(R.string.network_error_message),Toast.LENGTH_LONG)
                                .show();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(compositeDisposable!=null){
            compositeDisposable.clear();
        }
    }
}
