package com.vividseats.challenge.di;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vividseats.challenge.app.AppConstants;
import com.vividseats.challenge.networking.ApiService;
import com.vividseats.challenge.repositories.EventRepository;
import com.vividseats.challenge.repositories.EventRepositoryImp;
import com.vividseats.challenge.utils.CustomInterceptor;
import com.vividseats.challenge.utils.DateTypeAdapter;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class AppModule {

    private Context context;

    public AppModule(Context context){
        this.context = context;
    }

    @Provides
    @Singleton
    public Context getContext(){
        return this.context;
    }


    @Provides
    @Singleton
    public Gson getGson(){
        return new GsonBuilder()
                    .registerTypeAdapter(Date.class,new DateTypeAdapter())
                    .create();
    }

    @Provides
    @Singleton
    public OkHttpClient getOkHttpClient(){
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        return new OkHttpClient.Builder()
                .addNetworkInterceptor(new CustomInterceptor())
                .addNetworkInterceptor(loggingInterceptor)
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10,TimeUnit.SECONDS)
                .writeTimeout(10,TimeUnit.SECONDS)
                .build();
    }

    @Provides
    @Singleton
    public Retrofit geRetrofit(Gson gson, OkHttpClient okHttpClient){
        return new Retrofit.Builder()
                            .baseUrl(AppConstants.BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create(gson))
                            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                            .client(okHttpClient)
                            .build();
    }

    @Provides
    @Singleton
    public ApiService getApiService(Retrofit retrofit){
        return retrofit.create(ApiService.class);
    }

    @Provides
    public EventRepository getEventRepository(ApiService apiService){
        return new EventRepositoryImp(apiService);
    }


}
