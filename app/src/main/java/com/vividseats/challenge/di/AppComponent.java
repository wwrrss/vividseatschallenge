package com.vividseats.challenge.di;

import com.vividseats.challenge.activities.MainActivity;
import com.vividseats.challenge.fragments.SuggestedFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {
    void inject(MainActivity target);
    void inject(SuggestedFragment target);
}
