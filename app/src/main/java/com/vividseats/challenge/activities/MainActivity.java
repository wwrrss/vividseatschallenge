package com.vividseats.challenge.activities;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import com.vividseats.challenge.R;
import com.vividseats.challenge.app.CustomApp;
import com.vividseats.challenge.fragments.SuggestedFragment;
import com.vividseats.challenge.models.Card;
import com.vividseats.challenge.networking.ApiService;
import com.vividseats.challenge.repositories.EventRepository;
import com.vividseats.challenge.utils.EventsViewModelFactory;
import com.vividseats.challenge.viewmodels.EventsViewModel;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;


public class MainActivity extends AppCompatActivity {

    private final String ACTIVITY_NAME = MainActivity.class.getName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((CustomApp)getApplication()).getAppComponent().inject(this);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        Fragment suggestedFragment = new SuggestedFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.mainActivityFrame,suggestedFragment,"SUGGESTED").commit();

    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
