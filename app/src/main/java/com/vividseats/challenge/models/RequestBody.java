package com.vividseats.challenge.models;

import java.util.Date;

public class RequestBody {
    private Date startDate;
    private Date endDate;
    private Boolean includeSuggested;

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Boolean getIncludeSuggested() {
        return includeSuggested;
    }

    public void setIncludeSuggested(Boolean includeSuggested) {
        this.includeSuggested = includeSuggested;
    }

    @Override
    public String toString() {
        return "RequestBody{" +
                "startDate=" + startDate +
                ", endDate=" + endDate +
                ", includeSuggested=" + includeSuggested +
                '}';
    }
}
