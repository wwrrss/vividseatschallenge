package com.vividseats.challenge.app;

import android.app.Application;

import com.vividseats.challenge.di.AppComponent;
import com.vividseats.challenge.di.AppModule;
import com.vividseats.challenge.di.DaggerAppComponent;

public class CustomApp extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = createAppComponent();

    }

    private AppComponent createAppComponent(){
        return DaggerAppComponent
                .builder()
                .appModule(new AppModule(this))
                .build();
    }

    public AppComponent getAppComponent(){
        return appComponent;
    }
}
