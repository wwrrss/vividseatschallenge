package com.vividseats.challenge.repositories;



import com.vividseats.challenge.models.Card;
import com.vividseats.challenge.models.RequestBody;
import com.vividseats.challenge.networking.ApiService;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

public class EventRepositoryImp implements EventRepository {

    private ApiService apiService;

    private List<Card> cardList;

    private Subject<List<Card>> cardsSubject;

    private CompositeDisposable compositeDisposable;

    public EventRepositoryImp(ApiService apiService){
        this.apiService = apiService;
        compositeDisposable = new CompositeDisposable();
        cardList = new ArrayList<>();
        cardsSubject = PublishSubject.create();

    }

    @Override
    public Observable<List<Card>> getCardsObservable() {
        return cardsSubject;
    }

    @Override
    public void fetchNetworkData(){
        RequestBody requestBody = new RequestBody();
        requestBody.setStartDate(new Date());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_YEAR,10);
        requestBody.setEndDate(calendar.getTime());
        requestBody.setIncludeSuggested(true);
        apiService.getCards(requestBody)
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<Card>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(List<Card> cards) {
                        cardsSubject.onNext(cards);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if(!compositeDisposable.isDisposed()){
                            try{
                                cardsSubject.onError(e);
                            }catch (Exception e1){
                                //cardsSubject.onError(e1);
                            }

                        }

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void clear() {
        if(compositeDisposable!=null){
            compositeDisposable.clear();
            compositeDisposable.dispose();
        }
    }
}
