package com.vividseats.challenge.repositories;

import com.vividseats.challenge.models.Card;

import java.util.List;

import io.reactivex.Observable;

public interface EventRepository {
    Observable<List<Card>> getCardsObservable();
    void fetchNetworkData();
    void clear();
}
