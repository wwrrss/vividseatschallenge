package com.vividseats.challenge.viewmodels;

import android.arch.lifecycle.ViewModel;

import com.vividseats.challenge.models.Card;
import com.vividseats.challenge.repositories.EventRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;


public class EventsViewModel extends ViewModel {

    private EventRepository eventRepository;

    private Subject<List<Card>> cardsSubject;

    private CompositeDisposable compositeDisposable;


    public EventsViewModel(EventRepository eventRepository){
        this.eventRepository = eventRepository;
        compositeDisposable = new CompositeDisposable();
        cardsSubject = PublishSubject.create();
        subscribeToRepository();
        eventRepository.fetchNetworkData();

    }

    private void subscribeToRepository(){

       eventRepository.getCardsObservable()
                    .subscribe(new Observer<List<Card>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onNext(List<Card> cards) {
                            cardsSubject.onNext(cards);
                        }

                        @Override
                        public void onError(Throwable e) {
                            if(!compositeDisposable.isDisposed()){
                                cardsSubject.onError(e);
                            }
                        }

                        @Override
                        public void onComplete() {

                        }
                    });

    }

    public Observable<List<Card>> getCardsObservable(){
        return cardsSubject;
    }


    @Override
    protected void onCleared() {
        super.onCleared();
        if(compositeDisposable!=null){
            compositeDisposable.clear();
            compositeDisposable.dispose();
            eventRepository.clear();
        }

    }
}
