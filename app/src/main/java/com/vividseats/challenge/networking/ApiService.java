package com.vividseats.challenge.networking;

import com.vividseats.challenge.models.Card;
import com.vividseats.challenge.models.RequestBody;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiService {
    @POST("mobile/v1/home/cards")
    Observable<List<Card>> getCards(@Body RequestBody requestBody);
}
