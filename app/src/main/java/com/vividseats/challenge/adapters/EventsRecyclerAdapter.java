package com.vividseats.challenge.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vividseats.challenge.R;
import com.vividseats.challenge.models.Card;

import java.text.SimpleDateFormat;
import java.util.List;

public class EventsRecyclerAdapter extends RecyclerView.Adapter<EventsRecyclerAdapter.ViewHolder> {

    private List<Card> cardList;

    private SimpleDateFormat simpleDateFormatDeserialize = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");

    public EventsRecyclerAdapter(List<Card> cardList, Context context){
        this.cardList = cardList;
        this.context = context;
    }

    private Context context;

    public void addNewData(List<Card> newData){
        cardList.clear();
        cardList.addAll(newData);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_row,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Card card = cardList.get(position);
        holder.textViewTitle.setText(card.getTopLabel());
        holder.textViewMiddleLabel.setText(card.getMiddleLabel());
        holder.textViewDate.setText(simpleDateFormatDeserialize.format(card.getStartDate()));
        holder.textViewEventCount.setText(
                new StringBuffer()
                        .append(card.getEventCount().toString())
                        .append(" ")
                        .append(context.getResources().getString(R.string.events))
        );
        Picasso.get()
                .load(card.getImage())
                .fit()
                .into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return cardList.size();
    }



    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView textViewTitle;
        public ImageView imageView;
        public TextView textViewMiddleLabel;
        public TextView textViewDate;
        public TextView textViewEventCount;
        public ViewHolder(View view){
            super(view);
            textViewTitle = view.findViewById(R.id.txtTitle);
            imageView = view.findViewById(R.id.img);
            textViewMiddleLabel = view.findViewById(R.id.txtMiddleLabel);
            textViewDate = view.findViewById(R.id.txtEventDate);
            textViewEventCount = view.findViewById(R.id.txtEventCount);
        }
    }
}
